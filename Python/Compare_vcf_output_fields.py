"""
Author: Ronald Nieuwenhuis
Description: This script was used to parse a survivor merged vcf output. It is not designed to be multi purpose and was
run during development. Sometimes statements were commented off etc.
It relies on venn.py from:  https://github.com/tctianchi/pyvenn/blob/master/venn.py
"""
import os, copy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def collect_head_info(header_file_path, csv_out_path=None):
    """Collect info on the presence of tags in the collected headers file. This file contains the name of the caller
    prefixed with a ~ then the header on following lines. It writes the output as a truth table for every tag present
    in the collective of headers so a comparison of features embedded in the vcf is possible."""
    header_file_path = header_file_path
    with open(header_file_path) as header_file:
        all_text = header_file.read()
        entries = all_text.split('~')[1:]
        all_info = {}
        all_callers = []
        category_sets = {'GENERAL': [], 'INFO': [], 'ALT': [], 'FILTER': [], 'FORMAT': []}
        for entry in entries:
            all_fields = entry.split('\n')
            caller = all_fields[0].rstrip('.vcf')
            all_callers.append(caller)
            all_info[caller] = {'GENERAL': [], 'INFO': [], 'ALT': [], 'FILTER': [], 'FORMAT': []}
            for complete_field in all_fields[1:]:
                if 'fileformat' in complete_field:
                    split_ff = complete_field.split('=')
                    ff = split_ff[1]
                    all_info[caller]['GENERAL'].append(ff)
                elif complete_field.startswith('##') and '<' in complete_field and '>' in complete_field:
                    split_on_equal = complete_field.lstrip('##').split(
                        '=')  # so split_on_equal[0] contains field category
                    category = split_on_equal[0]
                    split_on_comma = split_on_equal[2].split(',')
                    field_name = split_on_comma[0]
                    if category in all_info[caller].keys():
                        all_info[caller][category].append(field_name)
                        if field_name not in category_sets[category]:
                            category_sets[category].append(field_name)
    for category, field_list in category_sets.items():
        category_sets[category] = sorted(field_list)
    all_sorted_fields = [field for category_set in sorted(category_sets.keys()) for field in
                         category_sets[category_set]]
    presence_table = 'Caller\t' + '\t'.join(all_sorted_fields) + '\n'
    for sv_caller in all_callers:
        presence_table += sv_caller + '\t'
        caller_fields = [field for category in all_info[sv_caller] for field in all_info[sv_caller][category]]
        for field in all_sorted_fields:
            if field in caller_fields:
                presence_table += '1\t'
            else:
                presence_table += '0\t'
        presence_table += '\n'
    if csv_out_path:
        with open(csv_out_path, mode='w') as csv_out:
            csv_out.write(presence_table)
    else: return presence_table


def parse_survivor_vcf_venn(survivor_vcf_path, csv_out_path=None):
    """Parse the vcf file and get input for venn function"""
    file_name = os.path.basename(survivor_vcf_path).rstrip('.vcf')
    venn_name = survivor_vcf_path.rstrip('.vcf') + '_venn.png'
    with open(survivor_vcf_path) as input_vcf:
        all_sup_vectors = {}
        for entry in input_vcf:
            if not entry.startswith('#'):
                info_field = entry.split('\t')[7]
                info = {}
                for field_and_value in info_field.split(';'):
                    field = field_and_value.split('=')[0]
                    value = field_and_value.split('=')[1]
                    info[field] = value
                if info['SUPP_VEC'] in all_sup_vectors.keys():
                    all_sup_vectors[info['SUPP_VEC']] += 1
                else:
                    all_sup_vectors[info['SUPP_VEC']] = 1
    number_of_files = 5
    caller_names_input_survivor = ['10X', 'Minimap2/Sniffles', 'Bionano Solve', 'LUMPY', 'NGMLR/Sniffles']
    scenarios = {bin(i).split('0b')[-1].zfill(number_of_files): 0 for i in range(1, (2**number_of_files - 1) + 1)}
    print(len(scenarios.keys()))
    print(sorted(scenarios.keys()))
    print(len(all_sup_vectors))
    print(sorted(all_sup_vectors.keys()))
    for sup_vector, count in all_sup_vectors.items():
        scenarios[sup_vector] = count
    print(len(scenarios.keys()))
    print(sorted(scenarios.keys()))
    print('\t'.join(sorted(scenarios.keys())))
    #To do venn:
    #save_venn_to_png(scenarios, number_of_files, caller_names_input_survivor, venn_name)
    # if csv_out_path:
    #     out_string = '\t'.join([str(scenarios[venn_scen]) for venn_scen in sorted(scenarios.keys())]) + '\n'
    #     with open(csv_out_path, mode='a') as csv:
    #          csv.write(out_string)
    #
    # print(all_sup_vectors)

def save_venn_to_png(venn_raw, number_of_files, files_names_list, venn_name):
    """Save an image of a venn diagram to png."""
    function_call = 'venn.venn' + str(number_of_files) + '(venn_raw, names=' + str(files_names_list) + ')'
    fig, ax = eval(function_call)  # Not a very neat way to do stuff, but hey it works right?! Since there is no native switch in python..
    fig.savefig(venn_name, bbox_inches='tight')
    plt.close()



def parse_survivor_vcf_for_type_stats(survivor_vcf_path, csv_path):
    """Get type stats from survivor vcf"""
    with open(survivor_vcf_path) as input_vcf:
        sv_type_count = {}
        for entry in input_vcf:
            if not entry.startswith('#'):
                split_entry = entry.split('\t')
                info_field = split_entry[7]
                file_specific_field = split_entry[-5:]
                sv_type_field = info_field.split(';')[3]
                sv_type = sv_type_field.split('=')[1]
                if sv_type in sv_type_count.keys():
                    sv_type_count[sv_type] += 1
                else:
                    sv_type_count[sv_type] = 0
    total_calls = sum([int(sv_type_count[key]) for key in sv_type_count.keys()])
    sv_type_percentage = {key:round(int(value)/total_calls*100, 1) for key, value in sv_type_count.items()}
    count_values = [int(sv_type_count[key]) for key in sorted(sv_type_count.keys())]
    ratio_values = [sv_type_percentage[key] for key in sorted(sv_type_percentage.keys())]
    header_p1 = '\t'.join(sorted(sv_type_count.keys()))
    header_p2 = '\t'.join(sorted(sv_type_percentage.keys()))
    header = header_p1 + '\t' + header_p2
    all_values = count_values + ratio_values
    text_to_write = header + '\n' + '\t'.join([str(i) for i in all_values]) + '\n'
    with open(csv_path, mode='a') as csv_out:
        csv_out.write(text_to_write)

def parse_survivor_vcf_classify_per_caller_stats(survivor_vcf_path):
    """Get numbers and figures per caller"""
    with open(survivor_vcf_path) as input_vcf:
        all_callers = ['Longranger', 'Minimap2/sniffles', 'Solve', 'LUMPY', 'Sniffles']
        sv_type_count = {'BND': 0,'DEL': 0, 'DUP': 0, 'INS': 0, 'INV': 0, 'NA': 0}
        per_caller_count = {'Longranger': dict(sv_type_count), 'Minimap2/sniffles': dict(sv_type_count),
                            'Solve': dict(sv_type_count), 'LUMPY': dict(sv_type_count), 'Sniffles': dict(sv_type_count)}
        for entry in input_vcf:
            if not entry.startswith('#'):
                split_entry = entry.split('\t')
                info_field = split_entry[7]
                caller_fields = split_entry[-5:]
                # cf_0_10x = caller_fields[0]
                # cf_1_mm2 = caller_fields[1]
                # cf_2_bn = caller_fields[2]
                # cf_3_lpy = caller_fields[3]
                # cf_4_snf = caller_fields[4]
                sv_type = info_field.split(';')[3].split('=')[1]
                supp_vec = info_field.split(';')[1].split('=')[1]
                for i, caller in enumerate(supp_vec, 0):
                    if int(caller):
                        key = all_callers[i]
                        if sv_type in per_caller_count[key].keys():
                            per_caller_count[key][sv_type] += 1


    for caller in sorted(per_caller_count.keys()):
        out_str = ''
        for sv_type in sorted(per_caller_count[caller].keys()):
            out_str = out_str + str(per_caller_count[caller][sv_type]) + '\t'
        print(caller + '\t' + out_str)

def parse_survivor_vcf_per_type_stats(survivor_vcf_path):
    """Collect numbers and figures per sv type."""
    with open(survivor_vcf_path) as input_vcf:
        support = {1: 0, 2: 0 , 3: 0, 4: 0, 5: 0}
        support_per_type = {'BND': dict(support), 'DEL': dict(support), 'DUP': dict(support), 'INS': dict(support),
                            'INV': dict(support), 'NA': dict(support)}
        for entry in input_vcf:
            if not entry.startswith('#'):
                split_entry = entry.split('\t')
                info_field = split_entry[7]
                supp_vec = info_field.split(';')[1].split('=')[1]
                sv_type = info_field.split(';')[3].split('=')[1]
                number_of_supports = supp_vec.count('1')
                support_per_type[sv_type][int(number_of_supports)] += 1
    for sv_type in sorted(support_per_type.keys()):
        out_str = ''
        for num_support in sorted(support_per_type[sv_type].keys()):
            out_str = out_str + str(support_per_type[sv_type][num_support]) + '\t'
        print(sv_type + '\t' + out_str)


def get_specific_entries(survivor_vcf_path):
    """Query specific entries from the survivor vcf file"""
    with open(survivor_vcf_path) as input_vcf:
        selected_entries = []
        all_sup_vectors = {}
        for entry in input_vcf:
            if not entry.startswith('#'):
                split_entry = entry.split('\t')
                info_field = split_entry[7]
                supp_vec = info_field.split(';')[1].split('=')[1]
                sv_type = info_field.split(';')[3].split('=')[1]
                number_of_supports = supp_vec.count('1')
                if sv_type == 'DEL' and number_of_supports == 4:

                    print(entry)

    #                 supp_vec = supp_vec[:2] + supp_vec[3:]
    #                 if supp_vec in all_sup_vectors.keys():
    #                     all_sup_vectors[supp_vec] += 1
    #                 else:
    #                     all_sup_vectors[supp_vec] = 1
    #
    # number_of_files = 4
    # caller_names_input_survivor = ['10X', 'Minimap2/Sniffles', 'LUMPY', 'NGMLR/Sniffles']
    # scenarios = {bin(i).split('0b')[-1].zfill(number_of_files): 0 for i in range(1, (2 ** number_of_files - 1) + 1)}
    # for sup_vector, count in all_sup_vectors.items():
    #     scenarios[sup_vector] = count
    # venn_name = '/mnt/scratch/nieuw133/Graduation/Cucumis_metuliferus/Sv_file_merging/Dels_venn.png'
    # save_venn_to_png(scenarios, number_of_files, caller_names_input_survivor, venn_name)

def get_lengths(survivor_vcf_path, csv_out_path=None):
    """Get the lengths from the survivor vcf."""
    csv_out_path = '/home/nieuw133/PycharmProjects/Graduation_scripts/All_lengths.tsv'
    with open(survivor_vcf_path) as input_vcf:
        all_callers = ['Longranger', 'Minimap2/sniffles', 'Solve', 'LUMPY', 'Sniffles']
        sv_length_list = {'BND': [], 'DEL': [], 'DUP': [], 'INS': [], 'INV': [], 'NA': []}
        per_caller_lengths = {'Longranger': copy.deepcopy(sv_length_list),
                              'Minimap2/sniffles': copy.deepcopy(sv_length_list),
                              'Solve': copy.deepcopy(sv_length_list),
                              'LUMPY': copy.deepcopy(sv_length_list),
                              'Sniffles': copy.deepcopy(sv_length_list)}

        for entry in input_vcf:
            if not entry.startswith('#'):
                split_entry = entry.split('\t')
                info_field = split_entry[7]
                supp_vec = info_field.split(';')[1].split('=')[1]
                sv_type = info_field.split(';')[3].split('=')[1]
                caller_fields = split_entry[-5:]
                for i, caller_binary in enumerate(supp_vec, 0):
                    if caller_binary == '1':
                        caller_format_field = caller_fields[i]
                        format_fields = caller_format_field.split(':')
                        length = format_fields[2]
                        per_caller_lengths[all_callers[i]][sv_type].append(length)

    with open(csv_out_path, mode='w') as write_out:
        for key in sorted(per_caller_lengths.keys()):
            for key_2 in sorted(per_caller_lengths[key]):
                for length in per_caller_lengths[key][key_2]:
                    # print(key + '\t' + key_2 + '\t' + str(length) + '\n')
                    write_out.write(key + '\t' + key_2 + '\t' + str(length) + '\n')






def main():
    """"""
    collect_head_info('/mnt/scratch/nieuw133/Test/Vcf_headers.txt', '/home/nieuw133/PycharmProjects/Graduation_scripts/Vcf_field_comparison.csv')
    parse_survivor_vcf_venn('/mnt/scratch/nieuw133/Graduation/Cucumis_metuliferus/Sv_file_merging/Merge_svs_10000.vcf', '/home/nieuw133/PycharmProjects/Graduation_scripts/All_scenarios.tsv')
    parse_survivor_vcf_for_type_stats('/mnt/scratch/nieuw133/Graduation/Cucumis_metuliferus/Sv_file_merging/Merge_svs_10000.vcf', '/home/nieuw133/PycharmProjects/Graduation_scripts/Sv_types_counts.tsv')
    parse_survivor_vcf_classify_per_caller_stats('/mnt/scratch/nieuw133/Graduation/Cucumis_metuliferus/Sv_file_merging/Merge_svs_750.vcf')
    parse_survivor_vcf_per_type_stats('/mnt/scratch/nieuw133/Graduation/Cucumis_metuliferus/Sv_file_merging/Merge_svs_750.vcf')
    get_specific_entries('/mnt/scratch/nieuw133/Graduation/Cucumis_metuliferus/Sv_file_merging/Merge_svs_750.vcf')
    get_lengths('/mnt/scratch/nieuw133/Graduation/Cucumis_metuliferus/Sv_file_merging/Merge_svs_750.vcf')

# The order of vcf files in survivor input is important!
# cat Vcf_files.txt
# 10x_collected_svs_filtered.vcf
# Kiwano_arcs_scaffolded_complete_vs_mel_ref_asm10_md.vcf
# Kiwano_bspq1_vs_mel_ref.vcf
# Kiwano_vs_melon_ref_PE.vcf
# Sniffles_ngmlr_kiwano_vs_mel_ref.vcf


if __name__ == "__main__":
    main()